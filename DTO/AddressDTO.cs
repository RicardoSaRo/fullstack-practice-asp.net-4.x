﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DTO
{
    public class AddressDTO
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="Please fill the address area.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please fill the Email area.")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Please provide a phone number.")]
        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        [Required(ErrorMessage = "A fax number is required.")]
        public string Fax { get; set; }

        [Required(ErrorMessage = "Please fill the map area.")]
        public string MapPathLarge { get; set; }

        [Required(ErrorMessage = "Please fill the map area.")]
        public string MapPathSmall { get; set; }
    }
}
