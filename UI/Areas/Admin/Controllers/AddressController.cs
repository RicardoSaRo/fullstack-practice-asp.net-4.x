﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using BLL;

namespace UI.Areas.Admin.Controllers
{
    public class AddressController : BaseController
    {
        AddressBLL bll = new AddressBLL();

        // GET: Admin/Address
        public ActionResult AddAddress()
        {
            AddressDTO dto = new AddressDTO();
            return View(dto);
        }
        [HttpPost]
        [ValidateInput(false)] //--> To prevent errors when saving the maps with the <iframe>
        public ActionResult AddAddress(AddressDTO model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ProcessState = General.Messages.EmptyArea;
            }
            else
            {
                if (bll.AddAddress(model))
                {
                    ViewBag.ProcessState = General.Messages.AddSuccess;
                    ModelState.Clear();
                    model = new AddressDTO();
                }
                else
                {
                    ViewBag.ProcessState = General.Messages.GeneralError;
                }
            }
            return View(model);
        }

        public ActionResult AddressList()
        {
            List<AddressDTO> dto = new List<AddressDTO>();
            dto = bll.GetAddresses();
            return View(dto);
        }

        public ActionResult UpdateAddress(int ID)
        {
            AddressDTO dto = new AddressDTO();
            dto = bll.GetAddressWithID(ID);
            return View(dto);
        }
        [HttpPost]
        [ValidateInput(false)] //--> To prevent errors when saving the maps with the <iframe>
        public ActionResult UpdateAddress(AddressDTO model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ProcessState = General.Messages.EmptyArea;
            }
            else
            {
                if (bll.UpdateAddress(model))
                {
                    ViewBag.ProcessState = General.Messages.UpdateSuccess;
                    ModelState.Clear();
                    model = new AddressDTO();
                }
                else
                    ViewBag.ProcessState = General.Messages.GeneralError;
            }
            return View(model);
        }

        public JsonResult DeleteAddress(int ID)
        {
            bll.DeleteAddress(ID);
            return Json("");
        }
    }
}