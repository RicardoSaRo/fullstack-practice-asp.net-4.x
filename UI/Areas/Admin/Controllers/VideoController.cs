﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DTO;

namespace UI.Areas.Admin.Controllers
{
    public class VideoController : BaseController
    {
        VideoBLL bll = new VideoBLL();

        // GET: Admin/Video
        public ActionResult AddVideo()
        {
            VideoDTO dto = new VideoDTO();
            return View(dto);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddVideo(VideoDTO model)
        {
            if (ModelState.IsValid)
            {
                if (model.OriginalVideoPath.Length <= 32)
                {
                    //--> Prevents exception if OriginalVideoPath is shorter than 32 characters
                    ViewBag.ProcessState = General.Messages.ShortPath;
                }
                else
                {
                    string path = model.OriginalVideoPath.Substring(32);
                    string mergelink = "https://www.youtube.com/embed/" + path;
                    model.VideoPath = String.Format(@"<iframe width=""300"" height=""200"" src=""{0}"" frameborder=""0"" allowfullscreen></iframe>", mergelink);
                    if (bll.AddVideo(model))
                    {
                        ViewBag.ProcessState = General.Messages.AddSuccess;
                        ModelState.Clear();
                        model = new VideoDTO();
                    }
                    else
                    {
                        ViewBag.ProcessState = General.Messages.GeneralError;
                    }
                }
            }
            else
            {
                ViewBag.ProcessState = General.Messages.EmptyArea;
            }
            return View(model);
        }

        public ActionResult VideoList()
        {
            List<VideoDTO> dto = new List<VideoDTO>();
            dto = bll.GetVideos();
            return View(dto);
        }

        public ActionResult UpdateVideo(int ID)
        {
            List<VideoDTO> dtolist = new List<VideoDTO>();
            dtolist = bll.GetVideos();
            VideoDTO dto = dtolist.First(x=>x.ID == ID);
            return View(dto);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateVideo(VideoDTO model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ProcessState = General.Messages.EmptyArea;
            }
            else
            {
                if (model.OriginalVideoPath.Length <= 32)
                {
                    //--> Prevents exception if OriginalVideoPath is shorter than 32 characters
                    ViewBag.ProcessState = General.Messages.ShortPath;
                }
                else
                {
                    string path = model.OriginalVideoPath.Substring(32);
                    string mergelink = "https://www.youtube.com/embed/" + path;
                    mergelink += String.Format(@"<iframe width=""300"" height=""200"" src=""{0}"" frameborder=""0"" allowfullscreen></iframe>", mergelink);
                    model.VideoPath = mergelink;
                    if (bll.UpdateVideo(model))
                    {
                        ViewBag.ProcessState = General.Messages.UpdateSuccess;
                        ModelState.Clear();
                        model = new VideoDTO();
                    }
                    else
                        ViewBag.ProcessState = General.Messages.GeneralError;
                }
            }
            return View(model);
        }

        public JsonResult DeleteVideo(int ID)
        {
            bll.DeleteVideo(ID);
            return Json("");
        }
    }
}