﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class UserDAO : PostContext
    {
        public UserDTO GetUserWithUsernameAndPassword(UserDTO model)
        {
            UserDTO dto = new UserDTO();
            List<T_User> list = db.T_User.Where(x => x.Username == model.Username && x.Password == model.Password).ToList();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    dto.Username = item.Username;
                    dto.IsAdmin = item.isAdmin;
                    dto.ImagePath = item.ImagePath;
                    dto.ID = item.ID;
                    dto.Email = item.Email;
                    dto.Name = item.NameSurname;
                    dto.Password = item.Password;
                }
            }
            return dto;
        }

        public int AddUser(T_User user)
        {
            try
            {
                db.T_User.Add(user);
                db.SaveChanges();
                return user.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserDTO> GetUsers()
        {
            List<T_User> list = db.T_User.Where(x => x.isDeleted == false).OrderBy(x => x.AddDate).ToList();
            List<UserDTO> dtolist = new List<UserDTO>();
            foreach (var item in list)
            {
                UserDTO dto = new UserDTO();
                dto.IsAdmin=item.isAdmin;
                dto.Username=item.Username;
                dto.Email = item.Email;
                dto.Name = item.NameSurname;
                dto.Password=item.Password;
                dto.ImagePath=item.ImagePath;
                dto.ID=item.ID;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public string DeleteUser(int iD)
        {
            try
            {
                T_User user = db.T_User.First(x => x.ID == iD);
                user.isDeleted = true;
                user.DeletedDate = DateTime.Now;
                user.LastUpdateDate = DateTime.Now;
                user.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                string imgpath = user.ImagePath;
                return imgpath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateUser(UserDTO model)
        {
            try
            {
                T_User user = db.T_User.First(x => x.ID == model.ID);
                string oldImagePath = user.ImagePath;
                user.NameSurname = model.Name;
                user.Username = model.Username;
                user.Password = model.Password;
                user.Email = model.Email;
                if (model.ImagePath != null)
                    user.ImagePath = model.ImagePath;
                user.isAdmin = model.IsAdmin;
                user.LastUpdateDate = DateTime.Now;
                user.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                return oldImagePath;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserDTO GetUserWithID(int iD)
        {
            T_User user = db.T_User.First(x => x.ID == iD);
            UserDTO dto = new UserDTO();
            dto.ID = user.ID;
            dto.Username = user.Username;
            dto.Name = user.NameSurname;
            dto.IsAdmin = user.isAdmin;
            dto.Password = user.Password;
            dto.Email=user.Email;
            dto.ImagePath = user.ImagePath;
            return dto;
        }
    }
}
