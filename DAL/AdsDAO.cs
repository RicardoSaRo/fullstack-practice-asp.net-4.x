﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AdsDAO : PostContext
    {
        public int AddAds(Ad ads)
        {
            try
            {
                db.Ads.Add(ads);
                db.SaveChanges();
                return ads.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AdsDTO> GetAds()
        {
            List<Ad> list = db.Ads.Where(x => x.isDeleted == false).OrderBy(x => x.AddDate).ToList();
            List<AdsDTO> dtolist = new List<AdsDTO>();
            foreach (var item in list)
            {
                AdsDTO dto = new AdsDTO();
                dto.ID = item.ID;
                dto.Name = item.Name;
                dto.Link = item.Link;
                dto.ImagePath = item.ImagePath;
                dto.ImageSize = item.Size;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public AdsDTO GetAdsWithID(int iD)
        {
            Ad ad = db.Ads.First(x=>x.ID == iD);
            AdsDTO dto = new AdsDTO();
            dto.ID = ad.ID;
            dto.ImagePath= ad.ImagePath;
            dto.ImageSize = ad.Size;
            dto.Link= ad.Link;
            dto.Name= ad.Name;
            return dto;
        }

        public string DeleteAds(int iD)
        {
            try
            {
                Ad ad = db.Ads.First(x => x.ID == iD);
                string imgpath = ad.ImagePath;
                ad.isDeleted = true;
                ad.DeletedDate = DateTime.Now;
                ad.LastUpdateDate = DateTime.Now;
                ad.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                return imgpath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateAds(AdsDTO model)
        {
            try
            {
                Ad ad = db.Ads.First(x=>x.ID == model.ID);
                string oldImagePath = ad.ImagePath;
                ad.Name = model.Name;
                ad.Link = model.Link;
                if (model.ImagePath != null)
                    ad.ImagePath = model.ImagePath;
                ad.Size = model.ImageSize;
                ad.LastUpdateDate = DateTime.Now;
                ad.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                return oldImagePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
