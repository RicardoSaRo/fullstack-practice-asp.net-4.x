﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class VideoDAO : PostContext
    {
        public int AddVideo(Video model)
        {
            db.Videos.Add(model);
            db.SaveChanges();
            return model.ID;
        }

        public List<VideoDTO> GetVideos()
        {
            List<Video> list = db.Videos.Where(x => x.isDeleted == false).OrderByDescending(x=>x.AddDate).ToList();
            List<VideoDTO> dtolist = new List<VideoDTO>();
            foreach (var item in list)
            {
                VideoDTO dto = new VideoDTO();
                dto.ID = item.ID;
                dto.Title = item.Title;
                dto.VideoPath = item.VideoPath;
                dto.OriginalVideoPath = item.OriginalVideoPath;
                dto.AddDate = item.AddDate;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public int UpdateVideo(Video dto)
        {
            try
            {
                Video video = db.Videos.First(x=>x.ID == dto.ID);
                video.Title = dto.Title;
                video.VideoPath = dto.VideoPath;
                video.OriginalVideoPath = dto.OriginalVideoPath;
                video.LastUpdateDate = dto.LastUpdateDate;
                video.LastUpdateUserID = dto.LastUpdateUserID;
                db.SaveChanges();
                return video.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteVideo(int iD)
        {
            try
            {
                Video video = db.Videos.First(x => x.ID == iD);
                video.isDeleted = true;
                video.DeletedDate = DateTime.Now;
                video.LastUpdateDate = DateTime.Now;
                video.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
