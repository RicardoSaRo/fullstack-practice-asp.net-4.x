﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class FavDAO : PostContext
    {
        public FavDTO GetFav()
        {
            FavLogoTitle fav = db.FavLogoTitles.First();
            FavDTO dto = new FavDTO();
            dto.ID = fav.ID;
            dto.Title = fav.Title;
            dto.FavPath = fav.Fav;
            dto.LogoPath = fav.Logo;
            return dto;
        }

        public FavDTO UpdateFav(FavDTO model)
        {
            try
            {
                FavLogoTitle fav = db.FavLogoTitles.First();
                FavDTO returndto = new FavDTO();
                returndto.FavPath = fav.Fav;
                returndto.LogoPath = fav.Logo;
                returndto.ID = fav.ID;
                if (model.FavPath != null)
                    fav.Fav = model.FavPath;
                if (model.LogoPath != null)
                    fav.Logo = model.LogoPath;
                fav.Title = model.Title;
                fav.LastUpdateDate = DateTime.Now;
                fav.LastUpdateUserID = UserStatic.UserID;
                return returndto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
