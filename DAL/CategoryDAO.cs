﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL
{
    public class CategoryDAO : PostContext
    {
        public int AddCategory(Category dto)
        {
            try
            {
                db.Categories.Add(dto);
                db.SaveChanges();
                return dto.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CategoryDTO> GetCategories()
        {
            List<Category> list = db.Categories.Where(x => x.isDeleted == false).OrderByDescending(x=>x.CategoryName).ToList();
            List<CategoryDTO> dtolist = new List<CategoryDTO>();
            foreach (var item in list)
            {
                CategoryDTO dto = new CategoryDTO();
                dto.ID = item.ID;
                dto.CategoryName = item.CategoryName;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public static IEnumerable<SelectListItem> GetCategoriesForDropDown()
        {
            IEnumerable<SelectListItem> categoryList = db.Categories.Where(x => x.isDeleted == false).OrderBy(x => x.CategoryName).Select(x => new SelectListItem()
            {
                Text = x.CategoryName,
                Value = SqlFunctions.StringConvert((double)x.ID)
            }).ToList();
            return categoryList;
        }

        public List<Post> DeleteCategory(int iD)
        {
            try
            {
                Category category = db.Categories.First(x => x.ID == iD);
                category.isDeleted = true;
                category.DeletedDate = DateTime.Now;
                category.LastUpdateDate = DateTime.Now;
                category.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                List<Post> postlist = db.Posts.Where(x => x.isDeleted == false && x.CategoryID == category.ID).ToList();
                return postlist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CategoryDTO GetCategoryWithID(int iD)
        {
            Category category = db.Categories.FirstOrDefault(x => x.ID == iD);
            CategoryDTO dto = new CategoryDTO();
            dto.ID = category.ID;
            dto.CategoryName = category.CategoryName;
            return dto;
        }

        public int UpdateCategory(CategoryDTO model)
        {
            try
            {
                Category category = db.Categories.First(x => x.ID == model.ID);
                category.CategoryName = model.CategoryName;
                category.LastUpdateDate = DateTime.Now;
                category.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                return category.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
