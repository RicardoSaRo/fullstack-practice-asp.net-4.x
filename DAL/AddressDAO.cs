﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AddressDAO : PostContext
    {
        public int AddAddress(Address dto)
        {
            try
            {
                db.Addresses.Add(dto);
                db.SaveChanges();
                return dto.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AddressDTO> GetAddresses()
        {
            List<Address> list = db.Addresses.Where(x=>x.isDeleted == false).OrderBy(x=>x.AddDate).ToList();
            List<AddressDTO> dtolist = new List<AddressDTO>();
            foreach (var item in list)
            {
                AddressDTO dto = new AddressDTO();
                dto.ID = item.ID;
                dto.Address = item.Address1;
                dto.Email = item.Email;
                dto.Phone1 = item.Phone;
                dto.Phone2 = item.Phone2;
                dto.Fax = item.Fax;
                dto.MapPathLarge = item.MapPathLarge;
                dto.MapPathSmall = item.MapPathSmall;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public int UpdateAddress(AddressDTO model)
        {
            try
            {
                Address address = db.Addresses.First(x => x.ID == model.ID);
                address.Address1 = model.Address;
                address.Email = model.Email;
                address.Phone = model.Phone1;
                address.Phone2 = model.Phone2;
                address.Fax = model.Fax;
                address.MapPathLarge = model.MapPathLarge;
                address.MapPathSmall = model.MapPathSmall;
                address.LastUpdateDate = DateTime.Now;
                address.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                return address.ID;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteAddress(int iD)
        {
            try
            {
                Address address = db.Addresses.First(x => x.ID == iD);
                address.isDeleted = true;
                address.DeletedDate = DateTime.Now;
                address.LastUpdateDate = DateTime.Now;
                address.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public AddressDTO GetAddressWithID(int iD)
        {
            Address address = db.Addresses.First(x => x.ID == iD);
            AddressDTO dto = new AddressDTO();
            dto.ID = address.ID;
            dto.Address = address.Address1;
            dto.Email = address.Email;
            dto.Phone1 = address.Phone;
            dto.Phone2 = address.Phone2;
            dto.Fax = address.Fax;
            dto.MapPathLarge= address.MapPathLarge;
            dto.MapPathSmall= address.MapPathSmall;
            return dto;
        }
    }
}
