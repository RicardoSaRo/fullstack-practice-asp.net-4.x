﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MetaDAO : PostContext
    {
        public int AddMeta(Meta meta)
        {
            try
            {
                db.Metas.Add(meta);
                db.SaveChanges();
                return meta.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MetaDTO> GetMetaData()
        {
            try
            {
                List<MetaDTO> list = new List<MetaDTO>();
                List<Meta> meta = db.Metas.Where(x=>x.isDeleted==false).OrderBy(x=>x.AddDate).ToList();
                foreach (var item in meta)
                {
                    MetaDTO dto = new MetaDTO();
                    dto.MetaID = item.ID;
                    dto.MetaContent = item.MetaContent;
                    dto.Name = item.Name;
                    list.Add(dto);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MetaDTO GetMetaWithID(int iD)
        {
            try
            {
                MetaDTO dto = new MetaDTO();
                Meta meta = db.Metas.First(x => x.ID==iD && x.isDeleted==false);
                dto.MetaID = meta.ID;
                dto.Name = meta.Name;
                dto.MetaContent = meta.MetaContent;
                return dto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteMeta(int iD)
        {
            try
            {
                Meta meta = db.Metas.First(x => x.ID == iD);
                meta.isDeleted = true;
                meta.DeletedDate = DateTime.Now;
                meta.LastUpdateDate = DateTime.Now;
                meta.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMeta(MetaDTO dto)
        {
            try
            {
                Meta meta = db.Metas.First(x => x.ID == dto.MetaID && x.isDeleted == false);
                if (meta.ID!=0)
                {
                    meta.ID = dto.MetaID;
                    meta.Name = dto.Name;
                    meta.MetaContent = dto.MetaContent;
                    meta.LastUpdateDate = DateTime.Now;
                    meta.LastUpdateUserID = UserStatic.UserID;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
