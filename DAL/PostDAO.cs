﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PostDAO : PostContext
    {
        public int AddPost(Post post)
        {
            try
            {
                db.Posts.Add(post);
                db.SaveChanges();
                return post.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddImage(PostImage item)
        {
            try
            {
                db.PostImages.Add(item);
                db.SaveChanges();
                return item.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddTag(PostTag item)
        {
            try
            {
                db.PostTags.Add(item);
                db.SaveChanges();
                return item.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PostDTO> GetPosts()
        {
            var postlist = (from p in db.Posts.Where(x => x.isDeleted == false)
                            join c in db.Categories on p.CategoryID equals c.ID
                            select new
                            {
                                ID = p.ID,
                                Title = p.Title,
                                CategoryName = c.CategoryName,
                                AddDate = p.AddDate
                            }).OrderByDescending(x => x.AddDate).ToList();
            List<PostDTO> dtolist = new List<PostDTO>();
            foreach (var item in postlist)
            {
                PostDTO dto = new PostDTO();
                dto.ID = item.ID;
                dto.Title = item.Title;
                dto.CategoryName = item.CategoryName;
                dto.AddDate = item.AddDate;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public PostDTO GetPostWithID(int iD)
        {
            Post post = db.Posts.First(x => x.ID == iD);
            Category category = db.Categories.First(x => x.ID == post.CategoryID);
            PostDTO dto = new PostDTO();
            dto.ID = post.ID;
            dto.Title = post.Title;
            dto.TopicName = post.TopicName;
            dto.ShortContent = post.ShortContent;
            dto.PostContent = post.PostContent;
            dto.CategoryID = post.CategoryID;
            dto.CategoryName = category.CategoryName;
            dto.Slider = post.Slider;
            dto.Area1 = post.Area1;
            dto.Area2 = post.Area2;
            dto.Area3 = post.Area3;
            dto.Notification = post.Notification;
            dto.SeoLink = post.SeoLink;
            dto.Language = post.Language;
            dto.AddDate = post.AddDate;
            return dto;
        }

        public List<PostImageDTO> GetPostImageWithID(int iD)
        {
            List<PostImage> list = db.PostImages.Where(x => x.PostID == iD && x.isDeleted == false).ToList();
            List<PostImageDTO> dtolist = new List<PostImageDTO>();
            foreach (var item in list)
            {
                PostImageDTO dto = new PostImageDTO();
                dto.PostID = item.ID;
                dto.ImagePath = item.ImagePath;
                dto.ID = item.ID;
                dtolist.Add(dto);
            }
            return dtolist;
        }

        public void UpdatePost(PostDTO model)
        {
            Post post = db.Posts.First(x => x.ID == model.ID);
            post.Title = model.Title;
            post.CategoryID = model.CategoryID;
            post.Language = model.Language;
            post.SeoLink = model.SeoLink;
            post.ShortContent = model.ShortContent;
            post.PostContent = model.PostContent;
            post.Area1 = model.Area1;
            post.Area2 = model.Area2;
            post.Area3 = model.Area3;
            post.Slider = model.Slider;
            post.Notification = model.Notification;
            post.LastUpdateDate = DateTime.Now;
            post.LastUpdateUserID = UserStatic.UserID;
            db.SaveChanges();
        }

        public List<PostImageDTO> DeletePost(int iD)
        {
            try
            {
                Post post = db.Posts.First(x => x.ID == iD);
                List<PostImage> imglist = db.PostImages.Where(x=>x.PostID == iD).ToList(); //--> to delete images
                List<PostTag> taglist = db.PostTags.Where(x => x.PostID == iD).ToList(); //--> To delete tags
                List<PostImageDTO> dtolist = new List<PostImageDTO>(); //--> To return images for file delete
                post.isDeleted = true;
                post.DeletedDate = DateTime.Now;
                post.LastUpdateDate = DateTime.Now;
                post.LastUpdateUserID= UserStatic.UserID;
                db.SaveChanges();
                foreach (var item in post.PostImages)
                {
                    PostImageDTO dto = new PostImageDTO();
                    item.isDeleted = true;
                    item.DeletedDate = DateTime.Now;
                    item.LastUpdateDate = DateTime.Now;
                    item.LastUpdateUserID = UserStatic.UserID;
                    dto.ImagePath = item.ImagePath;
                    dtolist.Add(dto);
                }
                db.SaveChanges();
                return dtolist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeletePostImage(int iD)
        {
            try
            {
                PostImage image = db.PostImages.First(x => x.ID == iD);
                string imgpath = image.ImagePath;
                image.isDeleted = true;
                image.DeletedDate = DateTime.Now;
                image.LastUpdateDate = DateTime.Now;
                image.LastUpdateUserID = UserStatic.UserID;
                return imgpath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteTags(int iD)
        {
            List<PostTag> list = db.PostTags.Where(x => x.isDeleted == false && x.PostID == iD).ToList();
            foreach (var item in list)
            {
                item.isDeleted = true;
                item.DeletedDate = DateTime.Now;
                item.LastUpdateDate= DateTime.Now;
                item.LastUpdateUserID = UserStatic.UserID;
            }
        }

        public string GetTagListWithID(int iD)
        {
            List<PostTag> list = db.PostTags.Where(x => x.PostID == iD && x.isDeleted == false).ToList();
            string tagtext = "";
            foreach (var item in list)
            {
                if (tagtext == "")
                {
                    tagtext += item.TagContent;
                }
                else
                {
                    tagtext += ',' + item.TagContent;
                }
            }
            return tagtext;
        }
    }
}
