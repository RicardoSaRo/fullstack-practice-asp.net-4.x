﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class SocialMediaDAO : PostContext
    {
        public int AddSocialMedia(SocialMedia social)
        {
            try
            {
                db.SocialMedias.Add(social);
                db.SaveChanges();
                return social.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SocialMediaDTO> GetSocialMedia()
        {
            List<SocialMediaDTO> dtolist = new List<SocialMediaDTO>();
            List<SocialMedia> list = db.SocialMedias.Where(x => x.isDeleted == false).OrderBy(x => x.AddDate).ToList();
            foreach (var item in list)
            {
                SocialMediaDTO dto = new SocialMediaDTO();
                dto.ID = item.ID;
                dto.Name = item.Name;
                dto.ImagePath = item.ImagePath;
                dto.Link = item.Link;
                dtolist.Add(dto);
            }
            return dtolist;

        }

        public SocialMediaDTO GetSocialMediaWithID(int iD)
        {
            SocialMedia social = db.SocialMedias.First(x => x.ID == iD);
            SocialMediaDTO dto = new SocialMediaDTO();
            dto.ID = social.ID;
            dto.Name = social.Name;
            dto.ImagePath = social.ImagePath;
            dto.Link = social.Link;
            return dto;
        }

        public string UpdateSocialMedia(SocialMediaDTO model)
        {
            try
            {
                SocialMedia social = db.SocialMedias.First(x => x.ID == model.ID && x.isDeleted == false);
                string oldImagePath = social.ImagePath;
                social.Name = model.Name;
                if (model.ImagePath != null)
                    social.ImagePath = model.ImagePath;
                social.Link = model.Link;
                social.LastUpdateDate = DateTime.Now;
                social.LastUpdateUserID = UserStatic.UserID;
                db.SaveChanges();
                return oldImagePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteSocialMedia(int iD)
        {
            try
            {
                SocialMedia social = db.SocialMedias.First(x => x.ID == iD);
                string imgpath = social.ImagePath;
                social.isDeleted = true;
                social.DeletedDate = DateTime.Now;
                social.LastUpdateDate = DateTime.Now;
                social.LastUpdateUserID = UserStatic.UserID;
                return imgpath;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
