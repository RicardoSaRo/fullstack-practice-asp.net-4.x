﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web.Mvc;

namespace BLL
{
    public class CategoryBLL
    {
        CategoryDAO dao = new CategoryDAO();
        PostBLL pbll = new PostBLL();
        public bool AddCategory(CategoryDTO model)
        {
            Category dto = new Category();
            dto.CategoryName = model.CategoryName;
            dto.AddDate = DateTime.Now;
            dto.LastUpdateDate = DateTime.Now;
            dto.LastUpdateUserID = UserStatic.UserID;
            int ID = dao.AddCategory(dto);
            LogDAO.AddLog(General.ProcessType.CategoryAdd,General.TableName.Category,ID);
            return true;
        }

        public static IEnumerable<SelectListItem> GetCategoriesForDropDown()
        {
            return CategoryDAO.GetCategoriesForDropDown();
        }

        public List<CategoryDTO> GetCategories()
        {
            return dao.GetCategories();
        }

        public CategoryDTO GetCategoryWithID(int iD)
        {
            return dao.GetCategoryWithID(iD);
        }

        public bool UpdateCategory(CategoryDTO model)
        {
            int ID = dao.UpdateCategory(model);
            LogDAO.AddLog(General.ProcessType.CategoryAdd,General.TableName.Category,ID);
            return true;
        }

        public List<PostImageDTO> DeleteCategory(int iD)
        {
            List<Post> postlist = dao.DeleteCategory(iD);
            LogDAO.AddLog(General.ProcessType.CategoryDelete, General.TableName.Category, iD);
            List<PostImageDTO> dtolist = new List<PostImageDTO>();
            foreach (var post in postlist)
            {
                List<PostImageDTO> imgs = new List<PostImageDTO>();
                imgs = pbll.DeletePost(post.ID);
                foreach (var item in imgs)
                {
                    PostImageDTO dto = new PostImageDTO();
                    dto.ImagePath = item.ImagePath;
                    dtolist.Add(dto);
                }
            }
            return dtolist;
        }
    }
}
