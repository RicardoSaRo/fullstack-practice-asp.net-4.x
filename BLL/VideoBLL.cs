﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class VideoBLL
    {
        VideoDAO dao = new VideoDAO();
        public bool AddVideo(VideoDTO model)
        {
            Video dto = new Video();
            dto.Title = model.Title;
            dto.VideoPath = model.VideoPath;
            dto.OriginalVideoPath = model.OriginalVideoPath;
            dto.AddDate = DateTime.Now;
            dto.LastUpdateDate = DateTime.Now;
            dto.LastUpdateUserID = UserStatic.UserID;
            int ID = dao.AddVideo(dto);
            LogDAO.AddLog(General.ProcessType.VideoAdd, General.TableName.Video, ID);
            return true;
        }

        public List<VideoDTO> GetVideos()
        {
            return dao.GetVideos();
        }

        public bool UpdateVideo(VideoDTO model)
        {
            Video dto = new Video();
            dto.ID = model.ID;
            dto.Title=model.Title;
            dto.VideoPath=model.VideoPath;
            dto.OriginalVideoPath = model.OriginalVideoPath;
            dto.LastUpdateDate = DateTime.Now;
            dto.LastUpdateUserID = UserStatic.UserID;
            int ID = dao.UpdateVideo(dto);
            LogDAO.AddLog(General.ProcessType.VideoUpdate,General.TableName.Video, ID);
            return true;
        }

        public void DeleteVideo(int iD)
        {
            dao.DeleteVideo(iD);
            LogDAO.AddLog(General.ProcessType.VideoDelete,General.TableName.Video, iD);
        }
    }
}
