﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class AddressBLL
    {
        AddressDAO dao = new AddressDAO();
        public bool AddAddress(AddressDTO model)
        {
            Address dto = new Address();
            dto.Address1 = model.Address;
            dto.Email = model.Email;
            dto.Phone = model.Phone1;
            dto.Phone2 = model.Phone2;
            dto.Fax = model.Fax;
            dto.MapPathLarge = model.MapPathLarge;
            dto.MapPathSmall = model.MapPathSmall;
            dto.AddDate = DateTime.Now;
            dto.LastUpdateDate = DateTime.Now;
            dto.LastUpdateUserID = UserStatic.UserID;
            int ID = dao.AddAddress(dto);
            LogDAO.AddLog(General.ProcessType.AddressAdd, General.TableName.Address,ID);
            return true;
        }

        public List<AddressDTO> GetAddresses()
        {
            return dao.GetAddresses();
        }

        public AddressDTO GetAddressWithID(int iD)
        {
            return dao.GetAddressWithID(iD);
        }

        public bool UpdateAddress(AddressDTO model)
        {
            int ID = dao.UpdateAddress(model);
            LogDAO.AddLog(General.ProcessType.AddressUpdate, General.TableName.Address, ID);
            return true;
        }

        public void DeleteAddress(int iD)
        {
            dao.DeleteAddress(iD);
            LogDAO.AddLog(General.ProcessType.AddressDelete, General.TableName.Address, iD);
        }
    }
}
