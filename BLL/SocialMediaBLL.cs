﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class SocialMediaBLL
    {
        SocialMediaDAO dao = new SocialMediaDAO();
        public bool AddSocialMedia(SocialMediaDTO model)
        {
            SocialMedia social = new SocialMedia();
            social.Name= model.Name;
            social.Link= model.Link;
            social.ImagePath= model.ImagePath;
            social.AddDate = DateTime.Now;
            social.LastUpdateDate = DateTime.Now;
            social.LastUpdateUserID= UserStatic.UserID;
            int ID = dao.AddSocialMedia(social);
            LogDAO.AddLog(General.ProcessType.SocialMediaAdd, General.TableName.Social, ID);

            
            return true;
        }

        public List<SocialMediaDTO> GetSocialMedia()
        {
            List<SocialMediaDTO> dtolist = new List<SocialMediaDTO>();
            dtolist = dao.GetSocialMedia();
            return dtolist;
        }

        public SocialMediaDTO GetSocialMediaWithID(int iD)
        {
            SocialMediaDTO dto = new SocialMediaDTO();
            dto = dao.GetSocialMediaWithID(iD);
            return dto;
        }

        public string UpdateSocialMedia(SocialMediaDTO model)
        {
            string oldImagePath = dao.UpdateSocialMedia(model);
            LogDAO.AddLog(General.ProcessType.SocialMediaUpdate,General.TableName.Social,model.ID);
            return oldImagePath;
        }

        public string DeleteSocialMedia(int iD)
        {
            string imgpath = dao.DeleteSocialMedia(iD);
            LogDAO.AddLog(General.ProcessType.SocialMediaDelete, General.TableName.Social, iD);
            return imgpath;
        }
    }
}
