﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class PostBLL
    {
        PostDAO dao = new PostDAO();
        public bool AddPost(PostDTO model)
        {
            Post post = new Post();
            post.Title = model.Title;
            post.TopicName = model.TopicName;
            post.PostContent = model.PostContent;
            post.ShortContent = model.ShortContent;
            post.Slider = model.Slider;
            post.Area1 = model.Area1;
            post.Area2 = model.Area2;
            post.Area3 = model.Area3;
            post.Notification = model.Notification;
            post.CategoryID = model.CategoryID;
            post.SeoLink = SeoLink.GenerateUrl(model.Title);
            post.Language = model.Language;
            post.AddDate = DateTime.Now;
            post.AddUserID = UserStatic.UserID;
            post.LastUpdateDate = DateTime.Now;
            post.LastUpdateUserID = UserStatic.UserID;
            int ID = dao.AddPost(post);
            LogDAO.AddLog(General.ProcessType.PostAdd,General.TableName.Post,ID);
            SavePostImages(model.PostImages, ID);
            AddTag(model.TagText, ID);
            return true;
        }

        private void AddTag(string tagText, int PostID)
        {
            string[] tags;
            tags = tagText.Split(',');
            List<PostTag> taglist = new List<PostTag>();
            foreach (var item in tags)
            {
                PostTag tag = new PostTag();
                tag.PostID = PostID;
                tag.TagContent = item;
                tag.AddDate = DateTime.Now;
                tag.LastUpdateDate= DateTime.Now;
                tag.LastUpdateUserID = UserStatic.UserID;
                taglist.Add(tag);
            }
            foreach (var item in taglist)
            {
                int tagID = dao.AddTag(item);
                LogDAO.AddLog(General.ProcessType.TagAdd, General.TableName.Tag, tagID);
            }
        }

        public void SavePostImages(List<PostImageDTO> list, int PostID)
        {
            List<PostImage> imagelist = new List<PostImage>();
            foreach (var item in list)
            {
                PostImage image = new PostImage();
                image.PostID = PostID;
                image.ImagePath = item.ImagePath;
                image.AddDate = DateTime.Now;
                image.LastUpdateDate = DateTime.Now;
                image.LastUpdateUserID = UserStatic.UserID;
                imagelist.Add(image);
            }
            foreach (var item in imagelist)
            {
                int imageID = dao.AddImage(item);
                LogDAO.AddLog(General.ProcessType.ImageAdd, General.TableName.Image, imageID);
            }
        }

        public List<PostDTO> GetPosts()
        {
            return dao.GetPosts();
        }

        public PostDTO GetPostWithID(int iD)
        {
            PostDTO dto = dao.GetPostWithID(iD);
            dto.Categories = CategoryBLL.GetCategoriesForDropDown();
            dto.PostImages = dao.GetPostImageWithID(iD);
            dto.TagText = dao.GetTagListWithID(iD);
            return dto;
        }

        public bool UpdatePost(PostDTO model)
        {
            model.SeoLink = SeoLink.GenerateUrl(model.Title);
            dao.UpdatePost(model);
            LogDAO.AddLog(General.ProcessType.PostUpdate,General.TableName.Post,model.ID);
            if (model.PostImages != null)
                SavePostImages(model.PostImages, model.ID);
            dao.DeleteTags(model.ID);
            AddTag(model.TagText, model.ID);
            return true;
        }

        public string DeletePostImage(int iD)
        {
            string imgpath = dao.DeletePostImage(iD);
            LogDAO.AddLog(General.ProcessType.ImageDelete, General.TableName.Image, iD);
            return imgpath;
        }

        public List<PostImageDTO> DeletePost(int iD)
        {
            List<PostImageDTO> imglist = dao.DeletePost(iD);
            LogDAO.AddLog(General.ProcessType.PostDelete, General.TableName.Post, iD);
            return imglist;
        }
    }
}
